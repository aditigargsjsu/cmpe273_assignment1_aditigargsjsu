from flask import Flask, abort
from flask import request
from model import db
from model import User
from model import CreateDB
from model import app as application
import simplejson as json
from sqlalchemy.exc import IntegrityError
import os

# initate flask app
app = Flask(__name__)

@app.route('/')
def index():
	return 'Hello World! Docker-Compose for Flask & Mysql\n'

#CHANGES TO INCLUDE METHODS
# Trying with requst forms and with postman instead of browser arguments
@app.route('/v1/expenses', methods=['POST'])
def insert_user():
        if request.is_json == False:
                json_data = json.loads(request.data)
        else:
                json_data = request.data
        database = CreateDB(hostname = 'localhost')
        db.create_all()
        try:
		user = User(json_data['name'], 
				json_data['email'], 
				json_data['category'],
                                json_data['description'],
                                json_data['link'],
                                json_data['estimated_costs'],
				json_data['submit_date'],
                                'pending',
                                '')		
		db.session.add(user)
		db.session.commit()
		user_id=user.id
		return json.dumps({'id':str(user.id),'name':user.name,'email':user.email,'category':user.category,'description':user.description,'link':user.link,'estimated_costs':user.estimated_costs,'submit_date':user.submit_date,'status':'pending','decision_date':''}),201
	except IntegrityError:
		return json.dumps({'status':False})

# this page called /v1/expenses/expense id can allow three methods - get, post and delete. 
@app.route('/v1/expenses/<expense_id>', methods=['GET', 'PUT', 'DELETE' ])
def expense_task(expense_id):
    if request.method == 'GET':
        try:
                user_get = User.query.filter_by(id=expense_id).first()
                return json.dumps({'id':str(user_get.id),'name':user_get.name,'email':user_get.email,'category':user_get.category,'description':user_get.description,'link':user_get.link,'estimated_costs':user_get.estimated_costs,'submit_date':user_get.submit_date,'status':'pending','decision_date':''})
        except AttributeError:
            abort(404)

            
    if request.method == 'DELETE':
        try:

            user_delete=User.query.filter_by(id=expense_id).delete()
            db.session.commit()
            return ('', 204)
        except AttributeError:
            abort(404)




    if request.method == 'PUT':
        if request.is_json == False:
                json_data = json.loads(request.data)
        else:
                json_data = request.data
        try:
            user_update=User.query.filter_by(id=expense_id).first()
            new_estimated_costs = json_data['estimated_costs']
            user_update.estimated_costs = new_estimated_costs
            db.session.commit()
            return json.dumps({'estimated_costs':user_update.estimated_costs}),202
        except AttributeError:
            abort(404)

@app.route('/info')
def app_status():
	return json.dumps({'server_info':application.config['SQLALCHEMY_DATABASE_URI']})

# run app service 
if __name__ == "__main__":
	app.run(host="localhost", port=5000, debug=True)
